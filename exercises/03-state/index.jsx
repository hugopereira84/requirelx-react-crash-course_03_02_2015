import React from 'react';

class HiddenMessage extends React.Component {
  constructor(){
    super();
    this.state = {label:'Hide', collapsed :false, msg: ' esta collapsed' }
    // TODO implement this.state
  }
  onClick(ev) {
    console.log(this.state.label);

    if(this.state.collapsed){
        this.setState({label:'Hide', collapsed :false, msg: ' esta collapsed' })
    }else{
        this.setState({label:'Show', collapsed :true, msg: 'nao esta collapsed'})
    }
    
    

    // TODO set a new state
    // TODO `label` should reflect the state of the component with "Hide" or "Show"
    // TODO collapsed should be toggled
  }
  render() {
    console.log(this.state.collapsed)
    let style = {
      display: this.state.collapsed ? 'none' : 'block'
    };

    return (<div>
      <button type='button' onClick={this.onClick.bind(this)} className='btn btn-default'>{this.state.label}</button>
      <div className='well' style={style}>{this.state.msg}</div>
    </div>);
  }
};

export default HiddenMessage;
