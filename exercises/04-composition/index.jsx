import React from 'react';

class List extends React.Component {
  


  render() {
    let lstItems = this.props.items;
    
    let items = lstItems.map(function(item) {
      return (<li className='list-group-item'>{item}</li>);
    });

    return (<ul className='list-group'>{items}</ul>);
  }
}

class Topics extends React.Component {

  render() {
    let items = [
      'info',
      'Is this going to work?',
      'info',
      'info'
    ];

    return (<div>
      <List items={items} />
    </div>)
  }
}

export default Topics;
