import React from 'react';

class Book extends React.Component {
  render() {
    return (<div>
        Name: {this.props.title}<br/>
        Read: {this.props.read ? 'Yes' : 'No'}
    </div>);
  }
};

Book.defaultProps = {
  'title':'Lalalal',
  'read' : true
};

class Library extends React.Component {
  render() {
    let read = true;
    let title = 'Professional Node.js: Building Javascript Based Scalable Software';

    return (<ul>
      <li><Book /></li> 
      <li><Book title='xxx' read={false} /></li> 
    </ul>);
  }
};

export default Library;
